﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Abstract;

namespace DDD.Repositories.Abstract
{
    // CRUD -> C*UD
    public interface IRepositoryCUD<T, TId> where T : class, IAggregateRoot
    {
        void Save(T entity);
        void Add(T entity);
        void Remove(TId id);
        
    }
}
