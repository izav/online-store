﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Entity.Infrastructure;
using DDD.Repositories.EF;

namespace DDD.Repositories.ContextStorage
{
    public class HttpDataContextStorageContainer : IDataContextStorageContainer 
    {
        private string _dataContextKey = "DataContext";

        public DbContextEF GetDataContext()
        {
            DbContextEF dbContext = null;
            if (HttpContext.Current.Items.Contains(_dataContextKey))
                dbContext = (DbContextEF)HttpContext.Current.Items[_dataContextKey];

            return dbContext;
        }

        public void Store(DbContextEF storeDataContext)
        {
            if (HttpContext.Current.Items.Contains(_dataContextKey))
                HttpContext.Current.Items[_dataContextKey] = storeDataContext;
            else
                HttpContext.Current.Items.Add(_dataContextKey, storeDataContext);  
        }

    }
}
