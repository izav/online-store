﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DDD.Repositories.Abstract;
using DDD.Domain.Entities;
using DDD.Repositories.ContextStorage;
using DDD.Repositories.QueryModel;
using System.IO;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using DDD.Domain.Basket;


namespace DDD.Repositories.EF
{
    public class ProductTitleRepository : IRepositoryCRUD<ProductTitle, int>
    {
        private DbContextEF context;
        private DbSet<ProductTitle> set;

        public ProductTitleRepository()
        {
            context = DataContextFactory.GetDataContext();
            set = context.ProductTitles;
        }


        // DbContext
        public string GetEntitySetName()
        {
            return "ProductTitles";
        }
        //---------------------------------
        public IEnumerable<State> GetProductStates()
        {
            return context.States;
        }
        //-------------------------------------------
        public IEnumerable<Country> GetProductCountries()
        {
            return context.Countries;
        }
        //---------------------------------------------------
        public IEnumerable<Category> GetProductCategories()
        {
            return context.Categories;
        }

        //-----------------------------------------------
        public IEnumerable<Brand> GetProductBrands()
        {
            return context.Brands;
        }
     
        //---------------------------------------------------
        public IQueryable<ProductTitle> GetDBSet()
        {
            DbContextEF context = DataContextFactory.GetDataContext();
            DbSet<ProductTitle> set = context.ProductTitles;
            return set;
        }

       //-------------------------------------------------
        public IQueryable<ProductTitle> FindAll()
        {
            return GetDBSet().Include("Brand").Include("Category");
        }
       //---------------------------------------------------
        public void ProcessOrder(IEnumerable<CartLine> lines)
        {

            var productForQuantityUpdate = lines.Join(context.ProductTitles,
                                     l => l.Id,
                                     p => p.Id,
                                     (l, p) =>
                                         new
                                         {
                                             ProductToUpdate = l,
                                             Product = p
                                         });


            foreach (var p in productForQuantityUpdate)
            {

                context.Entry(p.Product).Collection("ProductsQuantity");

                ProductQuantity pp = p.Product.ProductsQuantity.Where(s =>
                   s.ProductSizeId == p.ProductToUpdate.ProductSizeId &&
                    s.ProductColorId == p.ProductToUpdate.ProductColorId)
                    .FirstOrDefault();
                if (pp != null)
                {
                    int newValue = pp.Quantity - p.ProductToUpdate.Quantity;

                    if (newValue <= 0)
                        context.Entry(pp).State = EntityState.Deleted;
                    else
                    {
                        pp.Quantity = newValue;
                        context.Entry(pp).State = EntityState.Modified;
                    }

                }
                context.SaveChanges();

            }
        }
        //-------------------------------------------------------


        public IQueryable<ProductTitle> FindAllBy(int BrandId=0, int CategoryId=0)
        {

            if (BrandId == 0 && CategoryId == 0)
                return GetDBSet().Include("Brand").Include("Category");
            else
            {
              return  context.ProductTitles
                    .Where(p => (BrandId == 0 || p.Brand.Id == BrandId) &&
                           (CategoryId == 0 || p.Category.Id == CategoryId))
                           .Include("Brand").Include("Category");
            }
        }

       //---------------------------------------------------
        public  ProductTitle FindBy(int id)
        {
            { 
                ProductTitle p = GetDBSet().Where(b => b.Id == id)
                      .Include("ProductTitleImages")
                      .Include("Category")
                      .Include("Brand")
                      .Include("ProductsQuantity")
                      .FirstOrDefault<ProductTitle>();
                     p.ProductTitleImages =
                     p.ProductTitleImages.OrderBy(s => s.ImageOrderId).ToList();
                     return p;
            }
        }

        //----------------------------------------------- 
        public void Add(ProductTitle entity) { }
        public void Remove(ProductTitle entity) { }

        //--------------------------------------------
        public void Remove(int id) 
        {
            ProductTitle entity = context.ProductTitles.Find(id);

            context.Entry(entity).Collection(p => p.ProductsQuantity)
                                     .Load();
            context.Entry(entity).Collection(p => p.ProductTitleImages)
                                     .Load();

            foreach ( var item in entity.ProductsQuantity.ToList())
                context.Entry(item).State = EntityState.Deleted;

            foreach (var item in entity.ProductTitleImages.ToList())
                context.Entry(item).State = EntityState.Deleted;
            
            entity.Category = null;
            entity.Brand = null;
                    
           context.Entry(entity).State = EntityState.Deleted;
           context.SaveChanges();
        }
       
        //--------------------------------------------------
        public ProductTitleImage GetProductTitleMainImage(int id)
        {
            ProductTitle productTitle = context.ProductTitles.Find(id);
            context.Entry(productTitle).Collection(p => p.ProductTitleImages)
                                      .Load();
            if (productTitle == null) return null;

           ProductTitleImage image = productTitle
                                    .ProductTitleImages
                                    .Where(p => p.ImageOrderId == 1)
            .FirstOrDefault();
           
           return image;
        }

        //--------------------------------------------------
        public ProductTitleImage GetProductTitleImage(int id)
        {
            ProductTitleImage prod = context.ProductTitleImages
                               .Find(id);
                               

            if (prod != null && prod.ImageData == null) prod = null;
            return prod;

        }

        //---------------------------------------------------
        public IQueryable<ProductTitle> GetByCategory(int id)
        {
            return GetDBSet().Where(b => b.Id == id)
                       .Include("Category")
                       .Include("Brand")
                       .OrderBy(p => p.Brand.Name);
                     
        }

        //-----------------------------------------
        public void Save(ProductTitle entity)
        {
            // Find() vs Where()
            /* http://stackoverflow.com/questions/7348663/c-sharp-entity-framework-how-can-i-combine-a-find-and-include-on-a-model-obje
             * http://judeokelly.com/primer-on-selecting-data-using-entity-framework/.
             * 
             * http://stackoverflow.com/questions/14720143/are-find-and-where-firstordefault-equivalent */
            Category category = context.Categories.Find(entity.Category.Id);
            Brand brand = context.Brands.Find(entity.Brand.Id);

            entity.Category = category;
            entity.Brand = brand;

            ProductTitle productTitle = context.ProductTitles
                                         .Find(entity.Id) ?? context.ProductTitles.Add(entity);
            
            // new item
            if (productTitle == entity)
            {               
                 context.Entry(productTitle).State = EntityState.Added;
            }
            else //old item
            {
                context.Entry(productTitle).Collection(p => p.ProductsQuantity)
                                      .Load();
                context.Entry(productTitle).Collection(p => p.ProductTitleImages)
                                      .Load();
                context.Entry(productTitle).Reference(p => p.Category);
                context.Entry(productTitle).Reference(p => p.Brand);
                               
                context.Entry(productTitle).CurrentValues.SetValues(entity);
            

            context.Entry(productTitle).CurrentValues.SetValues(entity);

            // Foreign key vs. Independent associations in EF 4 article
            //http://www.ladislavmrnka.com/2011/05/foreign-key-vs-independent-associations-in-ef-4/
            //              
            // ---------    Quantity set   --------
            //Update quantity        
            var quantityForUpdate = 
                from p1 in entity.ProductsQuantity
                join p2 in productTitle.ProductsQuantity on p1.Id equals p2.Id
                where p1.Quantity != p2.Quantity
                select  new {productQuantity = p2,  Quantity = p1.Quantity};

             foreach (var p in quantityForUpdate) 
             {
                     p.productQuantity.Quantity = p.Quantity; 
             }

             //Insert quantity
            var QuantityForInsert = entity.ProductsQuantity
                            .Where(p => p.Id == 0 && p.Quantity > 0);
            foreach ( var q in QuantityForInsert.ToList())
            {
                        productTitle.ProductsQuantity.Add(q);
            }

            // Delete quantity
            var quantityForDelete = entity.ProductsQuantity
                                .Where(p => p.Id > 0 && p.Quantity == 0);
            foreach ( var q in quantityForDelete.ToList())
            {
                       var pp = productTitle.ProductsQuantity.Where(p => p.Id == q.Id).FirstOrDefault();
                        context.Entry(pp).State = EntityState.Deleted;
            }


            //-------------      Images      -------------------------            
            
            // Has Images to update
            if (entity.ProductTitleImages != null)
            {
            var imagesForUpdate =
                from p1 in entity.ProductTitleImages
                join p2 in productTitle.ProductTitleImages on p1.Id equals p2.Id
                where p1.ImageOrderId != p2.ImageOrderId
                select new { ProductTitle = p2, ImageOrderId = p1.ImageOrderId };
            

           
            foreach (var p in imagesForUpdate)
            {
                p.ProductTitle.ImageOrderId = p.ImageOrderId;
            }
            
            //ImagesForDelete
            var imagesForDelete =
                                     (from p in productTitle.ProductTitleImages
                                      join e in entity.ProductTitleImages on p.Id equals e.Id into pe
                                      from s in pe.DefaultIfEmpty()
                                      select new { ImageFromDB = p, ImageId = (s == null ? 0 : s.Id) })
                                     .Where(k => k.ImageId == 0)
                                     .Select(n => n.ImageFromDB);
                                           
                                       
                                
            foreach (var item in imagesForDelete.ToList())                
                            context.Entry(item).State = EntityState.Deleted;

            // ImagesForInsert
           var imagesSetForInsert = entity.ProductTitleImages
                                        .Where(p => p.Id == 0);

           foreach (var q in imagesSetForInsert.ToList())
            {
                productTitle.ProductTitleImages.Add(q);
            }
        }
        else
        {        // All image to delete
                if (productTitle.ProductTitleImages != null && productTitle.ProductTitleImages.Count() > 0)
                {
                       foreach( var item in productTitle.ProductTitleImages.ToList())
                       {
                           context.Entry(item).State = EntityState.Deleted;  
                       }
                }
           }

            productTitle.Category = category;
            productTitle.Brand = brand;

            context.Entry(productTitle).State = EntityState.Modified;

         }
                                  
            

            SaveChanges(context);
       }

        //----------------------------------------------------------------
        //Wrapper for SaveChanges adding the Validation Messages to the generated exception
       
        private void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                ); // Add the original exception as the innerException
            }
        }


        //==================================================================
       
        public ResponseByQuery FindBy(Query query)
        {
            ResponseByQuery response = new ResponseByQuery();

            IQueryable<ProductTitle>  products = context.ProductTitles          
                    .Where(p => p.Category.Id == query.CategoryId);


            if (query.BrandIds!= null && query.BrandIds.Length != 0) 
                  products = products.Where(p => query.BrandIds.Any(brandId => brandId == p.Brand.Id));


            if (query.ColorIds!= null && query.ColorIds.Length != 0)
                products = products
                                .Include("ProductsQuantity")
                                .Where( p =>  (p.ProductsQuantity.SelectMany (x =>
                                                  query.ColorIds.Where(y => x.ProductColorId == y))).Count() > 0);

            if (query.SizeIds!=null && query.SizeIds.Length != 0)
                products = products
                                .Include("ProductsQuantity").Include("Brand")
                                .Where(p => (p.ProductsQuantity.SelectMany(x =>
                                                  query.SizeIds.Where(y => x.ProductSizeId == y))).Count() > 0);

            response.Products = products;
         
            response.MatchBrands = products
                 .Select( p => p.Brand)
                 .Distinct()
                 .OrderBy( s => s.Name);

           

            response.MatchColors = context.ProductsQuantity.Where( p => 
                (p.ProductTitles.SelectMany( x => products.Where(y => x.Id == y.Id))).Count() > 0)
                .Select(p => p.ProductColorId).Distinct().ToArray();


            response.MatchSizes = context.ProductsQuantity.Where(p =>
                (p.ProductTitles.SelectMany(x => products.Where(y => x.Id == y.Id))).Count() > 0)
                .Select(p => p.ProductSizeId).Distinct().ToArray();

            return response;
        }        
    }

    
}
