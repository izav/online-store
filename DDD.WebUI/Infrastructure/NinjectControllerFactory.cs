﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;
using Ninject;
using System.Linq;
using Moq;
using System.Configuration;
using DDD.Domain.Entities;
using DDD.Domain.Abstract;
using DDD.Repositories.Abstract;
using DDD.Repositories.EF;
using DDD.Service.Admin;
using DDD.WebUI.Infrastructure.Abstract;
using DDD.WebUI.Infrastructure.Concrete;
using DDD.Domain.User;
using DDD.Service.User;
using DDD.Service.Product;
using DDD.Domain.Order;



namespace DDD.WebUI.Infrastructure
{
    // add to global.asax
    // ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
           


            AddBindingsDB();
        }

        protected override IController GetControllerInstance(RequestContext requestContext,
                                    Type controllerType)
        {
            // получение объекта контроллера из контейнера
            // используя его тип
            return controllerType == null
            ? null
            : (IController)kernel.Get(controllerType);
        }



        private void AddBindingsDB()
        {
           

            kernel.Bind<IRepositoryR<Category, int>>().To<CategoryRepository>();
            kernel.Bind<IRepositoryCRUD<Category, int>>().To<CategoryRepository>();

            kernel.Bind<IRepositoryR<Brand, int>>().To<BrandRepository>();
            kernel.Bind<IRepositoryCRUD<Brand, int>>().To<BrandRepository>();

            kernel.Bind<IRepositoryR<ProductTitle, int>>().To<ProductTitleRepository>();
            kernel.Bind<IRepositoryCRUD<ProductTitle, int>>().To<ProductTitleRepository>();


            kernel.Bind<IAdminServices<Category, int>>().To<AdminCategoryServices>();
            kernel.Bind<IAdminServices<Brand, int>>().To<AdminBrandServices>();
            kernel.Bind<IAdminServices<ProductTitle, int>>().To<AdminProductServices>();
            kernel.Bind<IProductCatalogSevices>().To<ProductCatalogServices>();

            EmailSettings emailSettings = new EmailSettings
            {
                WriteAsFile = bool.Parse(ConfigurationManager
                .AppSettings["Email.WriteAsFile"] ?? "false")
            };
            kernel.Bind<IOrderProcessor>()
                    .To<EmailOrderProcessor>()
                    .WithConstructorArgument("settings", emailSettings);


            // User
            kernel.Bind<IRepositoryCRUD<UserProfile, int>>().To<UserRepository>();
            kernel.Bind<IAdminServices<UserProfile, int>>().To<UserServices>();

           

            kernel.Bind<IAuthProvider>().To<FormsAuthProvider>();
            


        }
    }
}