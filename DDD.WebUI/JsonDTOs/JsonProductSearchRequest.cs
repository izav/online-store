﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Mvc;
using DDD.Service.Product;

namespace DDD.WebUI.JsonDTOs
{

    public class JsonProductSearchRequest
    {
        
        public int CategoryId { get; set; }
        
        public int[] ColorIds { get; set; }
       
        public int[] SizeIds { get; set; }
       
        public int[] BrandIds { get; set; }
        
        public ProductsSortBy SortBy { get; set; }
       
        public IEnumerable<JsonRefinementGroup> RefinementGroups { get; set; }
        
        public int Index { get; set; }

        public int ItemsPerPage { get; set; }
    }    




    
 /*[DataContract]
    [ModelBinder(typeof(JsonModelBinder))]
    public class JsonProductSearchRequest
    {
        [DataMember]
        public int CategoryId { get; set; }
        [DataMember]
        public int[] ColorIds { get; set; }
        [DataMember]
        public int[] SizeIds { get; set; }
        [DataMember]
        public int[] BrandIds { get; set; }
        [DataMember]
        public ProductsSortBy SortBy { get; set; }
        [DataMember]
        public IEnumerable<JsonRefinementGroup> RefinementGroups { get; set; }
        [DataMember]
        public int Index { get; set; }
    } */   

}
