﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.WebUI.ViewModels.Product
{
    public class ProductsQuantityViewModel
    {
        public int ColorId;
        public IEnumerable<SizeQuantityViewModel> SizesQuantity;
    }

    public class SizeQuantityViewModel
    {
        public int SizeId;
        public int Quantity;  
    }
}