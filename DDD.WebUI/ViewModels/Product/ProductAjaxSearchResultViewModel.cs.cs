﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Service.Product;
using DDD.Domain.Entities;

namespace DDD.WebUI.ViewModels.Product
{
    public class ProductAjaxSearchResultViewModel: BaseProductCatalogPageViewModel
    {

        public ProductAjaxSearchResultViewModel() 
        {
            RefinementGroups = new List<RefinementGroup>();
        }

        public string SelectedCategoryName { get; set; }
        public int SelectedCategory { get; set; }

        public IEnumerable<RefinementGroup> RefinementGroups { get; set; }

        
            public int Id { get; set; }   
            public string Name { get; set; }           
            public decimal Price { get; set; }                       
            public Brand Brand { get; set; }
    }
}
