﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Service.Model;
using DDD.Domain.Entities;

namespace DDD.WebUI.ViewModels.Admin
{  
    public class ProductEditAdminViewModel : BaseAdminPageViewModel 
    {
        public ProductEditAdminViewModel() {

        int sizesCount = Enum.GetNames(typeof(ProductSize)).Length;
        int colorsCount = Enum.GetNames(typeof(ProductColor)).Length;

        
        ArrayOfQuantity = new ProductQuantity[colorsCount, sizesCount]; 
     }
      
        public ProductTitle ProductTitle {get; set;}
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Brand> Brands { get; set; }
       
        public ProductQuantity[,] ArrayOfQuantity { get; set; }

        
        public void AddProductsQuantityToArray()
        {   
            foreach (ProductQuantity q in ProductTitle.ProductsQuantity)
            {
                ArrayOfQuantity[q.ProductColorId-1, q.ProductSizeId-1] = q;
            }
        
        }

        
    }
}