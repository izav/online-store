﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Domain.Entities;
using DDD.WebUI.ViewModels.Product;

namespace DDD.WebUI.ViewModels.Admin
{
    public class AdminHomePageViewModel : HomePageViewModel
    {
        public int CategoryId { get; set; }
        public int BrandId { get; set; }
        public string OrderBy { get; set; }
        public int NumberOfResultsPerPage { get; set; }
        public int CurrentPage { get; set; } 
        public int AllItemsFound { get; set; }
        public int TotalPages { get; set; }
        public IEnumerable<Brand> Brands { get; set; }
        
    }
            
}