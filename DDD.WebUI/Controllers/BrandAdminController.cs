﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using DDD.Domain.Entities;
using DDD.Service.Admin;

namespace DDD.WebUI.Controllers
{
    public class BrandAdminController : Controller
    {
        private IAdminServices<Brand, int> adminServices;


        public BrandAdminController(IAdminServices<Brand, int> adminServices)
        {
            this.adminServices = adminServices;
        }

        public ActionResult Index()
        {

            return View(adminServices.GetAll());

        }

        public ViewResult Create()
        {
            return View("Edit", new Brand() { Id = 0 });
        }


        public ActionResult Edit(int Id)
        {
            return View(adminServices.GetBy(Id));
        }


        [HttpPost]
        public ActionResult Edit(Brand brand)
        {
            if (ModelState.IsValid)
            {
                if (brand.Id == 0) adminServices.Add(brand);
                else adminServices.Save(brand);
                return RedirectToAction("Index");
            }
            else
            {
                return View(brand);
            }
        }


        [HttpPost]
        public ActionResult Delete(int Id)
        {
            Brand brand = adminServices.GetBy(Id);
            if (brand != null)
            {

                adminServices.Remove(Id);
            }
            return RedirectToAction("Index");
        }


    }
}
