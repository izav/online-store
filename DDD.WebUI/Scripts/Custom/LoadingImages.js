﻿document.addEventListener("DOMContentLoaded", init, false);

function init() {
    document.querySelector('#files').addEventListener('change', handleFileSelect, false);
    selDiv = document.querySelector("#selectedFiles");
}

function handleFileSelect(e) {

    if (!e.target.files || !window.FileReader) return;

    selDiv.innerHTML = "<table><tr></tr></table>";

    var files = e.target.files;
    var filesArr = Array.prototype.slice.call(files);
    filesArr.forEach(function (f) {
        if (!f.type.match("image.*")) {
            return;
        }

        var reader = new FileReader();
        reader.onload = function (e) {
            var html = "<td><img  width='50'  src='" + e.target.result + "'></td>";
            selDiv.innerHTML += html;
        }
        reader.readAsDataURL(f);

    });


}