﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.Service.Model
{
    public class ProductQtyUpdateRequest
    {
        public int ProductId { get; set; }
        public int NewQty { get; set; }
    }
}