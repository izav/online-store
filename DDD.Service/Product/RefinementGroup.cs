﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.Service.Product
{
    public class RefinementGroup
    {
        public string Name { get; set; }
        public int GroupId { get; set; }
        public IEnumerable<Refinement> Refinements { get; set; }
    }
}