﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Service.Model;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.Repositories.EF;
using DDD.Repositories.Abstract;


namespace DDD.Service.Product
{
    public interface IProductCatalogSevices
    {
        IRepositoryR<ProductTitle, int> Repository { get; }
        IEnumerable<ProductTitle> GetFeaturedProducts();
        ProductsByCategoryResponseModel
            GetProductsByCategory(ProductsByCategoryRequestModel request);
                     
        ProductTitle GetProduct(int id);

       IEnumerable<Category> GetAllCategories();
      
        
    }
}