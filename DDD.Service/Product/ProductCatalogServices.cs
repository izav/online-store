﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.Repositories.Abstract;
using DDD.Repositories.EF;
using DDD.Service.Model;
using DDD.Repositories.QueryModel;



namespace DDD.Service.Product
{
    public class ProductCatalogServices : IProductCatalogSevices
    {
        //-----------------------------------------------------------
        public IRepositoryR<ProductTitle, int> _productRepository;
        private readonly IRepositoryR<Category, int> _categoryRepository;

        //-----------------------------------------------------------------

        public ProductCatalogServices(IRepositoryR<ProductTitle, int> productRepository,
                                       IRepositoryR<Category, int> categoryRepository)
        {   _productRepository = productRepository;
            _categoryRepository = categoryRepository;
        }
    
        
        //--------------------------------------------------------------------
             public ProductTitleImage GetProductTitleMainImage(int id)
        {
            return ((ProductTitleRepository)_productRepository).GetProductTitleMainImage(id);

        }
        //---------------------------------------------------
        public ProductTitleImage GetProductTitleImage(int id)
        {
            return ((ProductTitleRepository)_productRepository).GetProductTitleImage(id);

        }
        //-------------------------------------------------------------
        public IRepositoryR<ProductTitle, int> Repository
                { get { return _productRepository; } }


        
        //-------------------------------------------------------
        public IEnumerable<ProductTitle> GetFeaturedProducts()
         {
           
             IEnumerable<ProductTitle> response = _productRepository.FindAll();
             return response;
         }

      
        //--------------------------------------------------------------------
        public ProductTitle GetProduct(int id)
        {
           
            ProductTitle productTitle = _productRepository.FindBy(id);
            return productTitle;
        }

     
        //----------------------------------------------------------
        public IEnumerable<Category> GetAllCategories()
        {           
            IEnumerable<Category> categories = _categoryRepository.FindAll();
            return categories;
        }

        //------------------------------------------------------------
        public IEnumerable<Brand> GetAllBrands()
        {
            IEnumerable<Brand> brands = ((ProductTitleRepository)_productRepository).GetProductBrands();
            return brands;
        }

    
        //---------------------------------------------------------------------
        public ProductsByCategoryResponseModel GetProductsByCategory(ProductsByCategoryRequestModel request)
        {         
             Query query = new Query();

             query.CategoryId = request.CategoryId;
             query.BrandIds = request.BrandIds;
             query.SizeIds = request.SizeIds;
             query.ColorIds = request.ColorIds;
            

             ProductsByCategoryResponseModel response = new ProductsByCategoryResponseModel();
             ResponseByQuery responseByQuery = _productRepository.FindBy(query);

             response.Products = responseByQuery.Products;
             
            //Sorting by Price
             switch (request.SortBy)
             {
                 case ProductsSortBy.PriceLowToHigh:
                     response.Products = response.Products.OrderBy(p => p.Price);
                     break;
                 case ProductsSortBy.PriceHighToLow:
                     response.Products = response.Products.OrderByDescending(p => p.Price);
                     break;
             }

             response.SelectedCategory = request.CategoryId;
             response.SelectedCategoryName =
                _categoryRepository.FindBy(request.CategoryId).Name;
             response.CurrentPage =  request.Index;
             response.NumberOfTitlesFound = response.Products.Count();
             response.TotalNumberOfPages = (int)Math.Ceiling((decimal)response.NumberOfTitlesFound / request.NumberOfResultsPerPage);
             

            //Refinement for menu
            List<Refinement> refinements;
            Refinement refinement;

            //Refinement by Brand                 
               refinements = new List<Refinement>();
               foreach (Brand brand in responseByQuery.MatchBrands)
              {
                  refinement = new Refinement() {  Id = brand.Id, Name = brand.Name };
                  refinements.Add(refinement);                  
              }
              RefinementGroup refinementGroup1 = new RefinementGroup()
              {
                  GroupId = 1,
                  Name = "Brand",
                  Refinements = refinements
              };

              //Refinement Size
             var sizes = responseByQuery.MatchSizes;
              refinements = new List<Refinement>();
              foreach (int sizeId in sizes)
              {
                  refinement = new Refinement() { Id = sizeId, Name = ((ProductSize)sizeId).ToString() };
                  refinements.Add(refinement);
              }
              RefinementGroup refinementGroup2 = new RefinementGroup()
              {
                  GroupId = 2,
                  Name = "Size",
                  Refinements = refinements
              };

              //Refinement by Color
              var colors = responseByQuery.MatchColors;
              refinements = new List<Refinement>();
              foreach (int colorId in colors)
              {
                  refinement = new Refinement() { Id = colorId, Name = ((ProductColor)colorId).ToString() };
                  refinements.Add(refinement);
              }
              RefinementGroup refinementGroup3 = new RefinementGroup()
              {
                  GroupId = 3,
                  Name = "Color",
                  Refinements = refinements
              };

              response.RefinementGroups = new List<RefinementGroup> { refinementGroup1, refinementGroup2, refinementGroup3 };
              response.Products = response.Products
                              .Skip((response.CurrentPage - 1) * request.NumberOfResultsPerPage)
                              .Take(request.NumberOfResultsPerPage);
           return response;
        }
     }
}

  