﻿
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.Repositories.Abstract;


namespace DDD.Service.Admin
{
    public interface IAdminServices<T,TKey>
    {
        

        T GetBy(TKey Id);
        IEnumerable<T> GetAll();
        void Save(T entity);
        void Add(T entity);
        void Remove(TKey id);
    }
}