﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Domain.CustomAttribute;
using System.ComponentModel.DataAnnotations;


namespace DDD.Domain.Entities
{
    public class UploadFileModel
    {
        [FileSize(10240)]
        [FileTypes("jpg,jpeg,png")]
        public HttpPostedFileBase File { get; set; }
    }
}