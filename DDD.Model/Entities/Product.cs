﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Abstract;


namespace DDD.Domain.Entities
{
    public class Product : EntityBase<int>, IAggregateRoot
    {

       

        
        public string Name
        {
            get { return Title.Name; }
        }

        public ProductTitle Title { get; set; }
        
       

        public Category Category
        {
            get {return Title.Category; }
        }

          public Brand Brand
        {
            get {return Title.Brand; }
        }


         

        

        public Decimal Price
        {
            get
            {
                if (Title == null) return 0M;
                 else return Title.Price; }
        }

       
    }
}
