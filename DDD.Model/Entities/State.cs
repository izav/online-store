﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Abstract;

namespace DDD.Domain.Entities
{
    public class State : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
        // public virtual ICollection<Profile> Profiles { get; set; }
        public static InitStateNames[] InitValue = {
      
                                             new InitStateNames() {Name1 =  "Alabama ", Name2 ="Alabama" },
                                             new InitStateNames() {Name1 =  "Alaska", Name2 =  "Alaska"}, 
											 new InitStateNames() {Name1 =  "Arizona", Name2 =  "Arizona"},
											 new InitStateNames() {Name1 =  "Arkansas", Name2 =  "Arkansas"}, 
											 new InitStateNames() {Name1 =  "California", Name2 =  "California"}, 
											 new InitStateNames() {Name1 =  "Colorado", Name2 =  "Colorado"}, 
											 new InitStateNames() {Name1 =  "Connecticut", Name2 =  "Connecticut"},
											 new InitStateNames() {Name1 =  "Delaware", Name2 =  "Delaware"},
											 new InitStateNames() {Name1 =  "District__of__Columbia", Name2 =  "District of Columbia"},
											 new InitStateNames() {Name1 =  "Florida", Name2 =  "Florida"},
											 new InitStateNames() {Name1 =  "Georgia", Name2 =  "Georgia"}, 
											 new InitStateNames() {Name1 =  "Hawaii", Name2 =  "Hawaii"}, 
											 new InitStateNames() {Name1 =  "Idaho", Name2 =  "Idaho"},
											 new InitStateNames() {Name1 =  "Illinois", Name2 =  "Illinois"}, 
											 new InitStateNames() {Name1 =  "Indiana", Name2 =  "Indiana"},
											 new InitStateNames() {Name1 =  "Iowa", Name2 =  "Iowa"}, 
											 new InitStateNames() {Name1 =  "Kansas", Name2 =  "Kansas"},
											 new InitStateNames() {Name1 =  "Kentucky", Name2 =  "Kentucky"},
											 new InitStateNames() {Name1 =  "Louisiana", Name2 =  "Louisiana"}, 
											 new InitStateNames() {Name1 =  "Maine", Name2 =  "Maine"}, 
											 new InitStateNames() {Name1 =  "Maryland", Name2 =  "Maryland"}, 
											 new InitStateNames() {Name1 =  "Massachusetts", Name2 =  "Massachusetts"}, 
											 new InitStateNames() {Name1 =  "Michigan", Name2 =  "Michigan"},
											new InitStateNames() {Name1 =  "Minnesota", Name2 =  "Minnesota"},
											 new InitStateNames() {Name1 =  "Mississippi", Name2 =  "Mississippi"},
											 new InitStateNames() {Name1 =  "Missouri", Name2 =  "Missouri"}, 
											 new InitStateNames() {Name1 =  "Montana", Name2 =  "Montana"}, 
											 new InitStateNames() {Name1 =  "Nebraska", Name2 =  "Nebraska"}, 
											 new InitStateNames() {Name1 =  "Nevada", Name2 =  "Nevada"}, 
											 new InitStateNames() {Name1 =  "New__Hampshire", Name2 =  "New Hampshire"}, 
											 new InitStateNames() {Name1 =  "New__Jersey", Name2 =  "New Jersey"}, 
											 new InitStateNames() {Name1 =  "New__Mexico", Name2 =  "New Mexico"}, 
											 new InitStateNames() {Name1 =  "New__York", Name2 =  "New York"}, 
											 new InitStateNames() {Name1 =  "North__Carolina", Name2 =  "North Carolina"}, 
											 new InitStateNames() {Name1 =  "North__Dakota", Name2 =  "North Dakota"}, 
											 new InitStateNames() {Name1 =  "Ohio", Name2 =  "Ohio"}, 
											 new InitStateNames() {Name1 =  "Oklahoma", Name2 =  "Oklahoma"},
											 new InitStateNames() {Name1 =  "Oregon", Name2 =  "Oregon"}, 
											 new InitStateNames() {Name1 =  "Pennsylvania", Name2 =  "Pennsylvania"}, 
											new InitStateNames() {Name1 =  " Rhode__Island", Name2 =  "Rhode Island"}, 
											 new InitStateNames() {Name1 =  "South__Carolina", Name2 =  "South Carolina"}, 
											  new InitStateNames() {Name1 =  "South__Dakota", Name2 =  "South Dakota"},
											  new InitStateNames() {Name1 =  "Tennessee", Name2 =  "Tennessee"}, 
											 new InitStateNames() {Name1 =  "Texas", Name2 =  "Texas"}, 
											 new InitStateNames() {Name1 =  "Utah", Name2 =  "Utah"}, 
											 new InitStateNames() {Name1 =  "Vermont", Name2 =  "Vermont"}, 
											 new InitStateNames() {Name1 =  "Virginia", Name2 =  "Virginia"}, 
											 new InitStateNames() {Name1 =  "Washington", Name2 =  "Washington"},
											 new InitStateNames() {Name1 =  "West__Virginia", Name2 =  "West Virginia"},
											 new InitStateNames() {Name1 =  "Wisconsin", Name2 =  "Wisconsin"},
											 new InitStateNames() {Name1 =  "Wyoming", Name2 =  "Wyoming"}, 
											 new InitStateNames() {Name1 =  "Outside_US", Name2 =  "Live outside the US"}};

        /*
         
										<select name="_QCB6_C" id="_Q0_C" class="mrDropdown" style="">
											<option style="" class="mrMultiple" value="">--Please select your answer--</option>
											<option style="" class="mrMultiple" value="Alabama">Alabama</option>
											<option style="" class="mrMultiple" value="Alaska">Alaska</option>
											<option style="" class="mrMultiple" value="Arizona">Arizona</option>
											<option style="" class="mrMultiple" value="Arkansas">Arkansas</option>
											<option style="" class="mrMultiple" value="California">California</option>
											<option style="" class="mrMultiple" value="Colorado">Colorado</option>
											<option style="" class="mrMultiple" value="Connecticut">Connecticut</option>
											<option style="" class="mrMultiple" value="Delaware">Delaware</option>
											<option style="" class="mrMultiple" value="District__of__Columbia">District of Columbia</option>
											<option style="" class="mrMultiple" value="Florida">Florida</option>
											<option style="" class="mrMultiple" value="Georgia">Georgia</option>
											<option style="" class="mrMultiple" value="Hawaii">Hawaii</option>
											<option style="" class="mrMultiple" value="Idaho">Idaho</option>
											<option style="" class="mrMultiple" value="Illinois">Illinois</option>
											<option style="" class="mrMultiple" value="Indiana">Indiana</option>
											<option style="" class="mrMultiple" value="Iowa">Iowa</option>
											<option style="" class="mrMultiple" value="Kansas">Kansas</option>
											<option style="" class="mrMultiple" value="Kentucky">Kentucky</option>
											<option style="" class="mrMultiple" value="Louisiana">Louisiana</option>
											<option style="" class="mrMultiple" value="Maine">Maine</option>
											<option style="" class="mrMultiple" value="Maryland">Maryland</option>
											<option style="" class="mrMultiple" value="Massachusetts">Massachusetts</option>
											<option style="" class="mrMultiple" value="Michigan">Michigan</option>
											<option style="" class="mrMultiple" value="Minnesota">Minnesota</option>
											<option style="" class="mrMultiple" value="Mississippi">Mississippi</option>
											<option style="" class="mrMultiple" value="Missouri">Missouri</option>
											<option style="" class="mrMultiple" value="Montana">Montana</option>
											<option style="" class="mrMultiple" value="Nebraska">Nebraska</option>
											<option style="" class="mrMultiple" value="Nevada">Nevada</option>
											<option style="" class="mrMultiple" value="New__Hampshire">New Hampshire</option>
											<option style="" class="mrMultiple" value="New__Jersey">New Jersey</option>
											<option style="" class="mrMultiple" value="New__Mexico">New Mexico</option>
											<option style="" class="mrMultiple" value="New__York">New York</option>
											<option style="" class="mrMultiple" value="North__Carolina">North Carolina</option>
											<option style="" class="mrMultiple" value="North__Dakota">North Dakota</option>
											<option style="" class="mrMultiple" value="Ohio">Ohio</option>
											<option style="" class="mrMultiple" value="Oklahoma">Oklahoma</option>
											<option style="" class="mrMultiple" value="Oregon">Oregon</option>
											<option style="" class="mrMultiple" value="Pennsylvania">Pennsylvania</option>
											<option style="" class="mrMultiple" value="Rhode__Island">Rhode Island</option>
											<option style="" class="mrMultiple" value="South__Carolina">South Carolina</option>
											<option style="" class="mrMultiple" value="South__Dakota">South Dakota</option>
											<option style="" class="mrMultiple" value="Tennessee">Tennessee</option>
											<option style="" class="mrMultiple" value="Texas">Texas</option>
											<option style="" class="mrMultiple" value="Utah">Utah</option>
											<option style="" class="mrMultiple" value="Vermont">Vermont</option>
											<option style="" class="mrMultiple" value="Virginia">Virginia</option>
											<option style="" class="mrMultiple" value="Washington">Washington</option>
											<option style="" class="mrMultiple" value="West__Virginia">West Virginia</option>
											<option style="" class="mrMultiple" value="Wisconsin">Wisconsin</option>
											<option style="" class="mrMultiple" value="Wyoming">Wyoming</option>
											<option style="" class="mrMultiple" value="Outside__US">Live outside the US</option>
         
         */
    }

    public class InitStateNames
    {
        public string Name1;
        public string Name2;
    }
}
