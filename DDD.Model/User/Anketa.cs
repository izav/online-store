﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DDD.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
namespace DDD.Domain.User
{

    public enum Education { BS = 1, MD = 2, PD = 3, School =4}
    public enum Occupation { IT = 1, Medical = 2, Music = 3, Actor = 4, Eductation =5 };
    public enum EyeColor { Blue =1, Green = 2, Gray =3, Brown = 4, Black = 5};
    public enum HairColor {  Brown = 1, Black = 2, Blonde =3, Gray =4};
    public enum MaritalStatus{ Single = 1, Married = 2, Separated = 3, Widow = 4 };
    public enum Children { One = 1, Two = 2, Three = 3, Four = 4 };
    

    public class Anketa : EntityBase<int>, IAggregateRoot, IProductAttribute
    {
       [HiddenInput(DisplayValue = false)]
       public string Name { get; set; }
        //-------------------------


       //  [UIHint(DisplayValue = false)]
         [DisplayName("Marital Status")]
        [HiddenInput(DisplayValue = false)]
       public int MaritalStatusId { get; set; }

        [NotMapped]
        [DisplayName("Marital Status")]
        public MaritalStatus MaritalStatusValue
        {
            get { return (MaritalStatus)MaritalStatusId; }
            set { MaritalStatusId = (int)value; }
        }
        //----------------------------------



       [DisplayName("Hair Color")]
        [HiddenInput(DisplayValue = false)]
       public int HairColorId { get; set; }


        [NotMapped]
        [DisplayName("Hair Color")]
        public HairColor HairColorValue
        {
            get { return (HairColor)HairColorId; }
            set { HairColorId = (int)value; }
        }

        //----------------------------------

         [DisplayName("Eye Color")]
        [HiddenInput(DisplayValue = false)]
        public int EyeColorId { get; set; }

        [NotMapped]
        [DisplayName("Eye Color")]
        public EyeColor EyeColorValue
        {
            get { return (EyeColor)EyeColorId; }
            set { EyeColorId = (int)value; }
        }

        //----------------------------------

       public int Height { get; set; }

       //-----------------------------------
[DisplayName("Education")]
       [HiddenInput(DisplayValue = false)]
       public int EducationId { get; set; }


       [NotMapped]
       [DisplayName("Education")]
       public Education EducationValue
       {
           get { return (Education)EducationId; }
           set { EducationId = (int)value; }
       }

       //----------------------------------
          [DisplayName("Occupation")]
        [HiddenInput(DisplayValue = false)]
       public int OccupationId { get; set; }


       [NotMapped]
       [DisplayName("Occupation")]
       public Occupation OccupationValue
       {
           get { return (Occupation)OccupationId; }
           set { OccupationId = (int)value; }
       }

        //----------------------------------
    }
}
