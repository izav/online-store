﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Entities;

namespace DDD.Domain.Basket
{
    public class Cart
    {

        private List<CartLine> lineCollection = new List<CartLine>();


        //-------------------------------------------
        public void AddItem(ProductTitle product, int colorId, int sizeId,  int quantity)
        {
            CartLine line = lineCollection
            .Where(p => p.Id == product.Id && p.ProductColorId ==colorId && p.ProductSizeId == sizeId)
            .FirstOrDefault();
            if (line == null)
            {
                lineCollection.Add(new CartLine()
                {
                    Id = product.Id,
                    Name = product.Name,
                    Price = product.Price,
                    BrandName = product.Brand.Name,
                    ProductColorId = colorId,
                    ProductSizeId = sizeId,
                    Quantity = quantity
                });

            }
            else
            {
                line.Quantity += quantity;
            }
        }


        //------------------------------------
        public void RemoveLine(ProductTitle product)
        {
            lineCollection.RemoveAll(l => l.Id == product.Id);
        }

        //---------------------------------------
        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Price * e.Quantity);
        }

        public int TotalItems()
        {
            return lineCollection.Sum(e => e.Quantity);
        }

        //------------------------------------
        public void Clear()
        {
            lineCollection.Clear();
        }

        //-----------------------------
        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }
    }

}

