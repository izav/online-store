﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using DDD.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DDD.Domain.Shipping
{
    

    public class ShippingDetails
    {
        [Required(ErrorMessage = "Please enter a name")]
        [RegularExpression("^((?!^First Name$)[a-zA-Z '])+$", ErrorMessage = "Name is required and must be properly formatted.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter the first address line")]
        public string Line1 { get; set; }

        public string Line2 { get; set; }
        public string Line3 { get; set; }

        [Required(ErrorMessage = "City is a Required field.")]
        [RegularExpression("^((?!^City$)[a-zA-Z '])+$", ErrorMessage = "City is required and must be properly formatted.")]
        public string City { get; set; }


        [DisplayName("State")]
        [Required(ErrorMessage = "State is a Required field.")]
        public int StateId { get; set; }

        [Required(ErrorMessage = "Zip is a Required field.")]
        [DataType(DataType.Text)]
        [RegularExpression("\\d{5}",
                ErrorMessage = "Zip Code is required and must be properly formatted.")]
        [Display( Name = "Zip")]
        public string Zip { get; set; }



        public bool GiftWrap { get; set; }

        public IEnumerable<State> States { get; set; }
       
    }
}