#  **Clothing store**  

**[izav.somee.com](http://izav.somee.com)**   
*( See below screenshots if the link doesn't work)*

---------------------------------------

**[Online clothing store](http://izav.somee.com)**, where customers can browse catalog products by categories, refine search by brands, sizes and colors, see details about items of interest, add them to shopping cart and proceed check-out.


 
**[Admin](http://izav.somee.com/ProductAdmin)**  provides support CRUD operations for products and their attributes. Catalog products can be described by category, brand, price, set of pictures and collection of quantity for different sizes and colors.  
Display previews of images before the user actually uploads them works in modern browsers and old ones (IE < 10) with limited access to local file system.



### Used software: ###

**Languages**

* C#, JavaScript

**Database**

* MS SQL Server 2014

**ORM**

* Entity Framework 4 (Code First)


**Internet**

* ASP.NET MVC 4, JavaScript, jQuery,  Templates, Ajax, CSS





_____________________________________________________________


# ** Screenshots** 



## **Front store**
______________________________________________________________

![frontstore1.jpg](https://bitbucket.org/repo/kMqke7A/images/167827265-frontstore1.jpg)

______________________________________________________________


## **Slide show**
______________________________________________________________


![FrontStore2.jpg](https://bitbucket.org/repo/kMqke7A/images/2059033178-FrontStore2.jpg)

______________________________________________________________




## **Admin/ Edit product**


*Display previews of images before the user actually uploads them works in modern browsers and old ones (IE < 10) with limited access to local file system.*   
______________________________________________________________

![A1.png](https://bitbucket.org/repo/kMqke7A/images/4291170102-A1.png)

______________________________________________________________

## **Admin**
______________________________________________________________

![Admin1.jpg](https://bitbucket.org/repo/kMqke7A/images/1230321667-Admin1.jpg)


______________________________________________________________